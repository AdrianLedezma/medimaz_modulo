import { createUserWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js';
import { getAuth } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js';
import { initializeApp } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js';

const firebaseConfig = {
  apiKey: "AIzaSyBPAseKAeTMFE4iPZLeELzzNOJRmhAAtyI",
  authDomain: "denmaz-dcc4b.firebaseapp.com",
  projectId: "denmaz-dcc4b",
  storageBucket: "denmaz-dcc4b.appspot.com",
  messagingSenderId: "599425474188",
  appId: "1:599425474188:web:7d604f456c5516e64625a9",
};
const app = initializeApp(firebaseConfig);
const auth = getAuth();

const registroForm = document.getElementById("registro");
const correoInput = document.getElementById("correo");
const contrasenaInput = document.getElementById("contrasena");
const confirmacionContrasenaInput = document.getElementById("ConfirmacionContrasena");

function validarContrasena() {
  var contrasena = document.getElementById("contrasena").value;
  var confirmarContrasena = document.getElementById("ConfirmacionContrasena").value;

  // Longitud mínima de 8 caracteres
  var longitudMinima = 8;

  // Expresiones regulares para verificar la presencia de letras minúsculas, mayúsculas, números y caracteres especiales
  var letrasMinusculas = '[a-z]';
  var letrasMayusculas = '[A-Z]';
  var numeros = '[0-9]';
  var caracteresEspeciales = '[!@#$%^&*()_+\\-=\\[\\]{};\'":\\\\|,.<>\\/?]+';  

  var esValida = true;
  var mensajeError = "";

  if (contrasena.length < longitudMinima) {
    mensajeError += "La contraseña debe tener al menos 8 caracteres.\n";
    esValida = false;
  }

  if (!contrasena.match(letrasMinusculas)) {
    mensajeError += "La contraseña debe contener al menos una letra minúscula.\n";
    esValida = false;
  }

  if (!contrasena.match(letrasMayusculas)) {
    mensajeError += "La contraseña debe contener al menos una letra mayúscula.\n";
    esValida = false;
  }

  if (!contrasena.match(numeros)) {
    mensajeError += "La contraseña debe contener al menos un número.\n";
    esValida = false;
  }

  if (!contrasena.match(caracteresEspeciales)) {
    mensajeError += "La contraseña debe contener al menos un carácter especial (por ejemplo, !@#$%^&*).\n";
    esValida = false;
  }

  if (contrasena !== confirmarContrasena) {
    mensajeError += "Las contraseñas no coinciden.\n";
    esValida = false;
  }

  if (esValida === false) {
    alert(mensajeError);
  }

  return esValida;
}


registroForm.addEventListener("submit", (event) => {
  event.preventDefault();

  // Validar contraseña antes de enviar el formulario
  if (validarContrasena() === false) {
    return; // Detener el envío del formulario si la contraseña no es válida
  }

  const correo = correoInput.value;
  const contrasena = contrasenaInput.value;

  createUserWithEmailAndPassword(auth, correo, contrasena)
    .then((userCredential) => {
      // Registro exitoso, redirigir a la página de inicio de sesión
      window.location.href = "/inicio_login";
    })
    .catch((error) => {
      if (error.code === 'auth/email-already-in-use') {
        alert("El correo electrónico ya está registrado. Por favor, utiliza otro correo electrónico.");
      } else {
        console.log("Error al registrar usuario: ", error.message);
      }
    });
});