const firebaseConfig = {
  apiKey: "AIzaSyBPAseKAeTMFE4iPZLeELzzNOJRmhAAtyI",
  authDomain: "denmaz-dcc4b.firebaseapp.com",
  projectId: "denmaz-dcc4b",
  storageBucket: "denmaz-dcc4b.appspot.com",
  messagingSenderId: "599425474188",
  appId: "1:599425474188:web:7d604f456c5516e64625a9",
};

import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js'


const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);

const emailInput = document.getElementById("Correo");
const passwordInput = document.getElementById("contraseña");
const signInButton = document.getElementById("btnEnviar");

signInButton.addEventListener("click", (event) => {
  event.preventDefault(); // Evita que el formulario se envíe automáticamente

  const Correo = emailInput.value;
  const contraseña = passwordInput.value;

  signInWithEmailAndPassword(auth, Correo, contraseña)
    .then((userCredential) => {
      window.location.href = "/inicio_login";
    })
    .catch((error) => {
      console.log("Error al iniciar sesión:", error);
      alert("Credenciales incorrectas. Por favor, intenta nuevamente."); 
    });
});